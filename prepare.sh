#/bin/sh
docker run -v `pwd`/data:/data -w /data  -it --rm --entrypoint ""  solanalabs/solana:v1.9.21 solana-keygen new -o validator-keypair.json  --force
docker run -v `pwd`/data:/data -w /data  -it --rm --entrypoint ""  solanalabs/solana:v1.9.21 solana-keygen new -o vote-account-keypair.json  --force
